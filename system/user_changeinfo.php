<?php
    session_start();
    if(isset($_SESSION['userid'])){
        if(isset($_POST['chinfo'])){
            include_once("functions.php");
            //Check password
            $pdo = new_db_connection();
            $query = "SELECT * FROM users WHERE id = :id";
            $sql = $pdo->prepare($query);
            $sql->bindParam(':id', $_SESSION['userid']);

            try{
                $sql->execute();
                $userinfo = $sql->fetch();
                $sql = null;
            } catch (Exception $e){
                die($e);
            }

            if(password_verify($_POST['password'], $userinfo['password'])){
                $query = "UPDATE users SET firstname = :firstname, lastname = :lastname, email = :email, gender=:gender WHERE id = :id";
                $sql = $pdo->prepare($query);
                $sql->bindParam(':firstname', $_POST['firstname']);
                $sql->bindParam(':lastname', $_POST['lastname']);
                $sql->bindParam(':email', $_POST['email']);
                $sql->bindParam(':id', $_SESSION['userid']);
                $sql->bindParam(':gender', $_POST['gender']);

                try {
                    $sql->execute();
                    $_SESSION['firstname'] = $_POST['firstname'];
                    $_SESSION['lastname'] = $_POST['lastname'];
                    $_SESSION['email'] = $_POST['email'];
                    $_SESSION['gender'] = $_POST['gender'];

                    header("Location: ../");
                    die();
                } catch(Exception $e){
                    die($e);
                }
            } else {
                die("Wrong password");
                header("Location: ../");
            }
        } else if(isset($_POST['goback'])){
            header("Location: ../");
        }
    } else {
        header("Location: ../");
    }
?>
