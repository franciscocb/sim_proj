<?php
    session_start();
    include_once("functions.php");

    if(isset($_SESSION['userid'])){
        $dbo = new_db_connection();
        $query = "INSERT INTO relations (u1, u2, status) VALUES (:u1, :u2, 2)";
        $sql = $dbo->prepare($query);
        $sql->bindParam(':u1', $_SESSION['userid']);
        $sql->bindParam(':u2', $_GET['id']);
        try {
            $sql->execute();
            
        } catch(Exception $e){
            die($e);
        }
        header("Location: ../?page=view&id=".$_GET['id']);
    } else {
        header("Location: ../");
    }

 ?>
