<?php
    session_start();
    include_once("functions.php");

    if(isset($_SESSION['userid'])){
        $dbo = new_db_connection();
        $query = "UPDATE relations SET status = 1 WHERE u1=:u1 AND u2=:u2";
        $sql = $dbo->prepare($query);
        $sql->bindParam(':u1', $_GET['id']);
        $sql->bindParam(':u2', $_SESSION['userid']);
        try {
            $sql->execute();
        } catch(Exception $e){
            die($e);
        }
        header("Location: ../");
    } else {
        header("Location: ../");
    }

 ?>
