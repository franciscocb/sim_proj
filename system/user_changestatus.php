<?php
    session_start();
    if(isset($_SESSION['userid'])){
        include_once("functions.php");
        //Check password
        $pdo = new_db_connection();
        $query = "SELECT * FROM users WHERE id = :id";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':id', $_SESSION['userid']);

        try{
            $sql->execute();
            $userinfo = $sql->fetch();
            $sql = null;
        } catch (Exception $e){
            die($e);
        }
        if(isset($_POST['changeStatus'])){
            $query = "UPDATE users SET current_status=:current_status WHERE id = :id";
            $sql = $pdo->prepare($query);
            $sql->bindParam(':current_status', $_POST['newStatus']);
            $sql->bindParam(':id', $_SESSION['userid']);

            try {
                if($_POST['newStatus'] == ''){
                    echo "Estado nao pode ser vazio! <a href='../index.php?page=changestatus'>Voltar</a>";
                    header("Location: ../");
                }else{
                    $sql->execute();
                    $_SESSION['current_status'] = $_POST['newStatus'];
                    
                    header("Location: ../");
                }
                die();
            } catch(Exception $e){
                die($e);
            }
        } else if(isset($_POST['goback'])){
            header("Location: ../");
        }else if(isset($_POST['deleteStatus'])){
            $query = "UPDATE users SET current_status = NULL WHERE id = :id";
            $sql = $pdo->prepare($query);
            $sql->bindParam(':id', $_SESSION['userid']);

            try{
                $sql->execute();
                $_SESSION['current_status'] = NULL;

                header("Location: ../");
            }catch(Exception $e){
                die("Nao foi possivel apagar o estado, contactar o administrador");
            } 
        }
    } else {
        header("Location: ../");
    }
?>
