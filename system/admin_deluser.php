<?php
    session_start();
    include_once("system/functions.php");

    if(isset($_SESSION['userid']) && $_SESSION['admin'] == 1){
        $pdo = new_db_connection();
        $query = "DELETE FROM users WHERE id = :id";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':id', $_GET['id']);

        try {
            $sql->execute();
        } catch (Exception $e){
            die("O utilizador não existe");
        }

        $query = "DELETE FROM relations WHERE u1 = :id OR u2 = :id";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':id', $_GET['id']);

        try {
            $sql->execute();

        } catch (Exception $e){
            die("Impossível elminar as relações deste utilizador");
        }

        header("Location: index.php?page=admin");
    } else header("Location: index.php");

 ?>
