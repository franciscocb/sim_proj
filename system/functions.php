<?php

	function getHtmlHead(){
		if($_GET['page'] != "view"){
			echo "<title>ULHTBook</title>";
		}
		echo '<link rel="stylesheet" href="style/layout.css" type="text/css"/>';
	}

	function getPage()
	{
		if(isset($_GET['page'])){
			if ($_GET['page'] == "register"){
				require ("pages/registar.php");
			} elseif($_GET['page'] == "changeinfo"){
				require("pages/user_changeinfo.php");
			} elseif($_GET['page'] == "changepic"){
				require("pages/user_changepic.php");
			} elseif($_GET['page'] == "changepwd"){
				require("pages/user_changepwd.php");
			} elseif($_GET['page'] == "view"){
				require("pages/user_view.php");
			} elseif ($_GET['page'] == "adduser"){
				require("pages/registar.php");
			} elseif ($_GET['page'] == "changestatus"){
				require("pages/user_changestatus.php");
			} elseif($_GET['page'] == "admin"){
				if($_GET['sub'] == "changeuserinfo"){
					require("pages/admin_changeuserinfo.php");
				} elseif($_GET['sub'] == "deleteuser"){
					require("system/admin_deluser.php");
				} else require("pages/admin_home.php");
			}
			else require ("pages/homepage.php");
		} else require ("pages/homepage.php");
	}

	function new_db_connection()
	{
		$servername = "localhost";
		$username = "sim_proj";
		$password = "sim_proj";
		$dbname = "sim_proj";

		try {
		    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
		    // set the PDO error mode to exception
		    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    //echo "Connected successfully";
			return $conn;
		}
		catch(PDOException $e)
	    {
	    	//echo "Connection failed: " . $e->getMessage();
			return null;
	    }
	}

	function getFullname($id)
	{
		$pdo = new_db_connection();
		$query = "SELECT firstname, lastname FROM users WHERE id = :id";
		$sql = $pdo->prepare($query);
		$sql->bindParam(':id', $id);

		try {
			$sql->execute();
			$userinfo = $sql->fetch();
			return $userinfo['firstname']." ".$userinfo['lastname'];
		} catch (Exception $e){
			return null;
		}
	}

	function relationStatus($id1, $id2)
	{
		$pdo = new_db_connection();
		$query = "SELECT * FROM relations";
		$sql = $pdo->prepare($query);
		try{
			$sql->execute();
			$amizade = $sql->fetchAll();

		}catch(Exception $e){
			die($e);
		}

		foreach ($amizade as $a){
			if($a['u1'] == $id1  && $a['u2'] == $id2){
				return $a['status'];
			} else if ($a['u2'] == $id1 && $a['u1'] == $id2 && $a['status'] == 1){
				return $a['status'];
			}
		}
		return false;
	}

	function session_handler()
	{
		echo "<div class='menu'>";
		if (isset($_SESSION['userid'])){
			echo '<img height="50px" width="50px" src="'.$_SESSION['pic_url'].'"/><br/><p style="margin: auto; text-align: center;">';
			if($_SESSION['admin'] == 1) echo "<a href='index.php?page=admin'>[ADMIN]</a> ";
			echo "Bem vindo ".$_SESSION['firstname']."<br/>";
			echo "<a href='index.php'>Homepage</a>";
			echo " | ";
            echo "<a href='?page=view&id=".$_SESSION['userid']."'>Ver Perfil</a>";
			echo " | ";
			echo "<a href='index.php?page=changeinfo'>Editar perfil</a>";
			echo " | ";
			echo "<a href='index.php?page=changepic'>Alterar foto</a>";
			echo " | ";
			echo "<a href='index.php?page=changepwd'>Alterar password</a>";
			echo " | ";
			if($_SESSION['current_status'] == NULL){
                    echo "<a href='index.php?page=changestatus'>Adicionar Estado</a> ";
            }else{
                    echo "<a href='index.php?page=changestatus'>Alterar Estado</a> ";

            }
			echo " | ";
			if ($_SESSION['admin'] == 1){
                echo "<a href='index.php?page=adduser'>Adicionar utilizador</a> ";
				echo " | ";
            }

			echo "<a href='system/login_handler.php?logout=1'>Logout</a></p>";

		} else {
			if(isset($_GET['error'])){
				echo "<p class='error'>Credenciais Inválidas</p>";
			}
            echo '<form style="margin: auto;" method="POST" action="system/login_handler.php">';
				echo "<label>Login </label>";
                echo '<input type="email" name="email" placeholder="Email"/>';
                echo '<input type="password" name="password" placeholder="Password"/>';
                echo '<input type="submit" name="login" value="Login"/>';
                echo '<input type="submit" name="register" value="Registar"/>';
            echo '</form>';
		}
		echo "</div>";
		return;
	}
?>
