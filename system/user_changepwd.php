<?php
    session_start();
    include_once("functions.php");

    if(isset($_SESSION['userid'])){
        if(isset($_POST['submit'])){
            if($_POST['new1'] == $_POST['new2']){
                $pdo = new_db_connection();
                $query = "SELECT * FROM users WHERE id = :id";
                $sql = $pdo->prepare($query);
                $sql->bindParam(':id', $_SESSION['userid']);

                try {
                    $sql->execute();
                    $userinfo = $sql->fetch();
                    $sql = null;

                    if(password_verify($_POST['old'], $userinfo['password'])){
                        //header("Location: ../");
                        $query = "UPDATE users SET password = :pwd WHERE id = :id";
                        $sql = $pdo->prepare($query);
                        $sql->bindParam(':pwd', password_hash($_POST['new1'], PASSWORD_DEFAULT));
                        $sql->bindParam(':id', $_SESSION['userid']);

                        $sql->execute();
                        header("Location: ../");
                        die();
                    } else {
                        header("Location: ../index.php?page=changepwd&error=1");
                        die();
                    }

                } catch(Exception $e){
                    die($e);
                }
            } else {
                header("Location: ../index.php?page=changepwd&error=2");
                die();
            }
        }

    } else {
        header("Location: ../");
    }
?>
