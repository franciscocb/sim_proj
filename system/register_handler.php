<?php
    include_once("functions.php");

    $pdo = new_db_connection();

    $query = "INSERT INTO users (email, pic_url, password, firstname, lastname, gender) VALUES (:email, :pic_url, :password, :first, :last, :gender)";
    $sql = $pdo->prepare($query);

    $sql->bindParam(':email', $_POST['email']);
    $sql->bindParam(':password', password_hash($_POST['password'], PASSWORD_DEFAULT));
    $sql->bindParam(':first', $_POST['firstname']);
    $sql->bindParam(':last', $_POST['lastname']);
    $sql->bindParam(':gender', $_POST['gender']);

    $default_pic="style/default.png";

    if($_POST['pic_url'] == ''){
        $sql->bindParam(':pic_url', $default_pic);
    }else{
        $sql->bindParam(':pic_url', $_POST['pic_url']);
    }

    try {
        $sql->execute();

    } catch (Exception $e){
        die("Cannot create user");
    }
    header("Location: ../");
 ?>
