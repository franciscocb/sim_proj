<?php
    session_start();
    include_once("functions.php");
    echo "ok";
    if(isset($_SESSION['userid'])){
        $pdo = new_db_connection();
        $query = "UPDATE users SET pic_url = :url WHERE id = :id";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':url', $_POST['pic_url']);
        $sql->bindParam(':id', $_SESSION['userid']);

        try {
            $sql->execute();
            $_SESSION['pic_url'] = $_POST['pic_url'];
            header("Location: ../");
            //echo "ok";
        } catch(Exception $e){
            die($e);
        }
    } else {
        header("Location: ../");
    }
?>
