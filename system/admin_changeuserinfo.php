<?php
    session_start();

    if ($_SESSION['admin'] == 1){
        include_once("functions.php");

        $pdo = new_db_connection();

        $query = "UPDATE users SET email = :email, firstname = :firstname, lastname = :lastname,
                gender = :gender, admin = :admin, current_status = :current_status WHERE id = :id";

        $sql = $pdo->prepare($query);
        $sql->bindParam(':email', $_POST['email']);
        $sql->bindParam(':firstname', $_POST['firstname']);
        $sql->bindParam(':lastname', $_POST['lastname']);
        $sql->bindParam(':gender', $_POST['gender']);
        $sql->bindParam(':admin', $_POST['admin']);
        $sql->bindParam(':current_status', $_POST['current_status']);
        $sql->bindParam(':id', $_POST['id']);

        try {
            $sql->execute();
        } catch (Exception $e){
            die($e);
        }
        header("Location: ../?page=admin");
    } else {
        header("Location: ../");
    }
?>
