<?php
    session_start();
    include_once("functions.php");

    $dbo = new_db_connection();
    $query = "DELETE FROM relations WHERE u1 = :u1 AND u2 = :user";
    $sql = $dbo->prepare($query);
    $sql->bindParam(':u1', $_GET['u1']);
    $sql->bindParam(':user', $_SESSION['userid']);
    try {
        $sql->execute();
    } catch(Exception $e){
        die($e);
    }
    header("Location: ../");

 ?>
