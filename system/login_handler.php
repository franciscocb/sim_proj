<?php
    session_start();
    include_once("functions.php");

    if(!isset($_SESSION['userid'])){

        if(isset($_POST['login'])){
            $pdo = new_db_connection();
            $query = "SELECT * FROM users WHERE email = :email";
            $sql = $pdo->prepare($query);
            $sql->bindParam(':email', $_POST['email']);

            try{
                $sql->execute();
                $user = $sql->fetch();

                if(password_verify($_POST['password'], $user['password'])){
                    $_SESSION['userid'] = $user['id'];
                    $_SESSION['firstname'] = $user['firstname'];
                    $_SESSION['lastname'] = $user['lastname'];
                    $_SESSION['pic_url'] = $user['pic_url'];
                    $_SESSION['admin'] = $user['admin'];
                    $_SESSION['current_status'] = $user['current_status'];
                    $_SESSION['gender'] = $user['gender'];
                    header("Location: ../");
                } else {
                    header("Location: ../index.php?error");
                    die();
                }
            } catch (Exception $e) {
                header("Location: ../index.php?error");
                die();
            }

        } elseif(isset($_POST['register'])){
            header("Location: ../index.php?page=register");
        } else {
            header("Location: ../");
        }
    }
    else if(isset($_GET['logout'])) {
        session_destroy();
        header("Location: ../");
    } else {
        header("Location: ../");
    }
 ?>
