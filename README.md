# ULHTBook #
## Projecto para Sistemas de Informação e Multimédia em PHP ##
Trabalho realizado por:
Francisco Castel-Branco (21300935)
Luís Cardoso (21500092)
Sérgio Mendonça (21604755)

### Base de dados ###
A base de dados implementa duas tabelas: 'users' e 'relations'

#### A tabela 'users' tem o registo dos utilizadores. Estes têm: ####
* id > Auto incrementável
* email > Único
* pic_url > Imagem de perfil
* password > Codificada pela função 'password_hash()' do PHP
* firstname > Nome próprio
* lastname > Apelido
* admin > Define se o utilizador é Administrador
* current_status > Define o estado do utilizador;
* gender > Masculino ou Feminino

#### A tabela 'relations' define as relações entre utilizadores: ####
* u1 > Utilizador que fez o pedido de amizade
* u2 > Utilizador que recebeu o pedido de amizade
* status > 0 - Pedido inexistente | 1 - Pedido aceite | 2 - Pedido enviado


### Ficheiros ###

#### / ####
* index.php > Ficheiro de entrada no site que redirecciona para onde vamos.
* database.sql > Base de dados
* ToDo.txt > Lista de afazeres
* README.md > Este ficheiro

#### /pages ####
* admin_changeuserinfo.php > Formulário de alteração das informações de um utilizador (ADMIN)
* admin_home.php > Página de administração
* homepage.php > Página inicial do site
* registar.php > Formulário de registo
* user_changeinfo.php > Página de alteração de informação do utilizador
* user_changepic.php > Alterar a imagem do utilizador
* user_changepwd.php > Alterar a password do utilizador
* user_changestatus.php > Altera o estado do utilizador
* user_view.php > Página que mostra a informação de qualquer utilizador

#### /style ####
* layout.css > Código de layout da página (CSS)
* default.png > Imagem default dos utilizadores

#### /system ####
* admin_changeuserinfo.php > Altera as informações de um utilizador como Administrador
* admin_deluser.php > Apaga um utilizador
* friendship_accept.php > Método para aceitar um pedido de amizade
* friendship_ask.php > Método para pedir alguém em Amizades
* friendship_delete.php > Método para eliminar a amizade da BD
* friendship_refuse.php > Método para recusar um pedido de amizade
* functions.php > Ficheiro onde estão as funções de gestão do site assim como a de ligação à base de dados
* login_handler.php > Ficheiro que gere o formulário de login do site
* register_handler.php > Regista o utilizador na base de dados
* user_changeinfo.php > Método para alterar a informação do utilizador na BD
* user_changepic.php > Alterar a imagem do utilizador
* user_changepwd.php > Alterar a password do utilizador
* user_changestatus.php > Altera o estado do utilizador