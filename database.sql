DROP DATABASE IF EXISTS sim_proj;
CREATE DATABASE sim_proj;
USE sim_proj;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `pic_url` varchar(120) NOT NULL,
  `password` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `admin` tinyint(1) DEFAULT '0',
  `current_status` varchar(1024) DEFAULT NULL,
  `gender` enum('Masculino','Feminino','Outro') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `relations`;
CREATE TABLE `relations` (
  `u1` mediumtext,
  `u2` mediumtext,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*
	USERNAME= admin@admin.com
	PASSWORD: 123
*/

INSERT INTO users
VALUES (NULL, 'admin@admin.com', 'style/default.png', '$2y$10$SCSZuALZrSE6XEtY/m63o.fJP/uBfOq2KYUv.jasx6OTimYTff5UC', 'Administrador', 'Admin', 1, NULL, 'Masculino');
