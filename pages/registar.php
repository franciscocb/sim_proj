<?php
    session_start();
    include_once("system/functions.php");

    if (isset($_SESSION['userid']) && $_SESSION['admin'] == 0){
        header("Location: .");
        die();
    }

    $gender = $row['gender'];

?>
<html>
    <head>
        <?php getHtmlHead();?>
    </head>
    <body>
        <div id="container">
            <?php if($_SESSION['admin'] == 1) session_handler(); ?>
            Registar
            <form name="register" method="POST" action="system/register_handler.php">
                <input type="text" name="firstname" placeholder="Primeiro nome" required/>
                <input type="text" name="lastname" placeholder="Ultimo nome" required/>
                <select  id="gender" name="gender">
                    <option value="Masculino" <?php if($gender==1) { ?> selected="selected" <?php } ?>> Masculino</option>
                    <option value="Feminino" <?php if($gender==2) { ?> selected="selected" <?php } ?> >Feminino</option>
                </select>
                <input type="email" name="email" placeholder="Email" required/>
                <input type="password" name="password" placeholder="Password" required/>
                <input type="submit" value="Registar"/>
            </form>
        </div>
    </body>
</html>
