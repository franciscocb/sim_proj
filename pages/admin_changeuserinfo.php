<?php
    session_start();
    include_once("system/functions.php");

    if(!$_SESSION['admin'] == 1){
        header("Location: index.php");
    }

    $pdo = new_db_connection();
    $query = "SELECT * FROM users WHERE id = :id";
    $sql = $pdo->prepare($query);
    $sql->bindParam(':id', $_GET['id']);

    try {
        $sql->execute();
        $userinfo = $sql->fetch();
        $gender = $row['gender'];
    } catch (Exception $e){
        die($e);
    }

?>

<html>
    <head>
        <?php getHtmlHead();?>
    </head>
    <body>
        <div id="container">
            <?php
                session_handler();
                echo "<a href='index.php?page=view&id=".$userinfo['id']."'>Ver perfil</a>";
            ?>
            <form method="POST" action="system/admin_changeuserinfo.php">
                <?php echo '<input type="text" hidden value='.$userinfo['id'].' name="id"/>';?>
                <table>
                    <tr>
                        <td>Primeiro Nome</td>
                        <?php echo "<td><input type='text' value='".$userinfo['firstname']."' name='firstname' placeholder='".$userinfo['firstname']."'/></td>"; ?>
                    </tr>
                    <tr>
                        <td>Ultimo Nome</td>
                        <?php echo "<td><input type='text' value='".$userinfo['lastname']."' name='lastname' placeholder='".$userinfo['lastname']."'/></td>"; ?>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <?php echo "<td><input type='text' value='".$userinfo['email']."' name='email' placeholder='".$userinfo['email']."'/></td>"; ?>
                    </tr>
                        <tr>
                            <td>Sexo:</td>
                            <td>
                                <select  id="gender" name="gender">
                                    <option value="Masculino" <?php if($userinfo['gender']=="Masculino") { ?> selected="selected" <?php } ?>> Masculino</option>
                                    <option value="Feminino" <?php if($userinfo['gender']=="Feminino") { ?> selected="selected" <?php } ?> >Feminino</option>
                                    <option value="Outro" <?php if($userinfo['gender']=="Outro") { ?> selected="selected" <?php } ?> >Outro</option>
                                </select>
                            </td>
                        </tr>
                    <tr>
                        <td>Imagem</td>
                        <?php echo "<td><input type='text' value='".$userinfo['pic_url']."' name='pic_url' placeholder='".$userinfo['pic_url']."'/></td>"; ?>
                    </tr>
                    <tr>
                        <td>Admin</td>
                        <td>
                            <select id="admin" name="admin">
                                <option value="1" <?php if($userinfo['admin'] == 1) { ?> selected="selected" <?php } ?>> Sim</option>
                                <option value="0" <?php if($userinfo['admin'] == 0) { ?> selected="selected" <?php } ?>>Não</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <?php
                            if ($userinfo['current_status'] == ""){
                                echo "<td><input type='text' name='current_status' placeholder='Sem estado'/></td>";
                            } else {
                                echo "<td><input type='text' value='".$userinfo['current_status']."' name='current_status' placeholder='".$userinfo['current_status']."'/></td>";
                            }
                        ?>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center><input type="submit" value="Gravar" /></center>
                        </td>
                </table>
            </form>
        </div>
    </body>
</html>
