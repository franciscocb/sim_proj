<?php
    session_start();
    include_once("system/functions.php");
    $pdo = new_db_connection(); // queima se não fizer isto
 ?>
<html>
    <head>
        <?php getHtmlHead();?>
    </head>
    <body>
        <div id="container">
            <?php
            session_handler();
            if(isset($_SESSION['userid'])){
                if($_SESSION['current_status'] == NULL){
                    echo "Não tens nenhum estado";
                }else{
                    echo "O que estás a fazer: ".$_SESSION['current_status']."<br/>";
                }


                // Ver pedidos de amizade --------------------
                $query = "SELECT * FROM relations WHERE u1 = :userid OR u2 = :userid";
                $sql = $pdo->prepare($query);
                $sql->bindParam(':userid', $_SESSION['userid']);

                try {
                    $sql->execute();

                    $result = $sql->fetchAll();

                    $temPedidos = false;
                    $headerExists = false;
                    foreach($result as $r){

                        if($r['status'] == 2 && $r['u2'] == $_SESSION['userid']){
                            $temPedidos = true;
                        }
                        if($temPedidos && !$headerExists){
                            $headerExists = true;
                            echo "<table class='list'>";
                            echo "<tr><th>Pedido de amizade</th><th>Opções</th></tr>";
                        }
                        if($r['status'] == 2 && $r['u2'] == $_SESSION['userid']){
                            echo "<tr>";
                            echo "<td><a href='?page=view&id=".$r['u1']."'>".getFullname($r['u1'])."</a></td>";
                            echo "<td><a href='system/friendship_accept.php?id=".$r['u1']."'>Aceitar</a> ";
                            echo "<a href='system/friendship_refuse.php?u1=".$r['u1']."'>Cancelar</a></td>";
                            echo "</tr>";
                        }
                    }
                    echo "</table>";

                    $n_amigos = 0;
                    echo "<table class='list'>";
                    echo "<tr><th>Amigos</th></tr>";
                    foreach($result as $r){
                        if($r['status'] == 1){
                            echo "<tr>";
                            if($_SESSION['userid'] == $r['u1']){
                                echo "<td><a href='?page=view&id=".$r['u2']."'>".getFullname($r['u2'])."</a></td>";
                            } else {
                                echo "<td><a href='?page=view&id=".$r['u1']."'>".getFullname($r['u1'])."</a></td>";
                            }
                            echo "</tr>";
                            $n_amigos++;
                        }
                    }
                    echo "</table>";
                    if ($n_amigos == 0){
                        echo "Não tens amigos";
                    } else if ($n_amigos > 1){
                        echo "Tens ".$n_amigos." amigos";
                    } elseif ($n_amigos == 1){
                        echo "Tens ".$n_amigos." amigo";
                    }
                    echo "<br/>";

                } catch (Exception $e){
                    die($e);
                }
            }

            $query = "SELECT * FROM users";
            $sql = $pdo->prepare($query);

            $sql->execute();
            $result = $sql->fetchAll();
            echo "<table class='list'>";
            echo "<tr><th>Utilizadores</th></tr>";
            foreach ($result as $r) {
                echo "<tr><td><a href='?page=view&id=".$r['id']."'>".$r['firstname']." ".$r['lastname']."</a></td></tr>";
            }
            echo "</table>";
            ?>
        </div>
    </body>
</html>
