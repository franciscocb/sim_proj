<?php
    session_start();
    include_once("system/functions.php");

    if(isset($_SESSION['userid'])){
?>
<html>
    <head>
        <?php getHtmlHead();?>
    </head>
    <body>
        <div id="container">
            <?php
                session_handler();
                echo '<br/><img height="100px" width="100px" src="'.$_SESSION['pic_url'].'"/>';
            ?>
            <form method="POST" action="system/user_changepic.php">
                <input type="text" placeholder="URL" name="pic_url" required />
                <input type="submit" value="Alterar"/>
            </form>
        </div>
    </body>
</html>
<?php
    } else {
        header("Location: ../");
    }
 ?>
