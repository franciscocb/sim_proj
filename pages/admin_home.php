<?php
    session_start();
    if ($_SESSION['admin'] == 1){
        include_once("system/functions.php");
        $pdo = new_db_connection();
?>
<html>
    <head>
        <?php getHtmlHead();?>
    </head>
    <body>
        <div id="container">
            <?php
            session_handler();

            $query = "SELECT * FROM users";
            $sql = $pdo->prepare($query);

            $sql->execute();
            $result = $sql->fetchAll();
            echo "<table class='list'>";
            echo "<tr><th>Utilizadores</th></tr>";
            foreach ($result as $r) {
                echo "<tr>";
                echo "<td><a href='?page=view&id=".$r['id']."'>".$r['firstname']." ".$r['lastname']."</a></td>";
                echo "<td><a href='?page=admin&sub=changeuserinfo&id=".$r['id']."'>Editar</a>";
                echo " <a href='?page=admin&sub=deleteuser&id=".$r['id']."'>Eliminar</a></td>";
                echo "</tr>";
            }
            echo "</table>";
            ?>
        </div>
    </body>
</html>
<?php
    } else {
        header("Location: index.php");
    }
?>
