<?php
    session_start();
    include_once("system/functions.php");

    if(isset($_SESSION['userid'])){

?>
<html>
    <head>
        <?php getHtmlHead();?>
    </head>
    <body>
        <div id="container">
            <?php
                session_handler();
                if($_SESSION['current_status'] == NULL){
                    echo "O que estas a fazer: <i>Sem estado</i>";
                }else{
                    echo "O que estas a fazer: ".$_SESSION['current_status'];
                }
            ?>
             <form method="POST" action="system/user_changestatus.php">
                <table>
                    <tr>
                        <td>Novo Estado:</td>
                        <td><input type="text" placeholder="Introduza um estado" name="newStatus"/></td>
                    </tr>

                </table>
                <input type="submit" name="changeStatus" value="Mudar Estado"/>
                <input type="submit" name="deleteStatus" value="Apagar Estado"/>
                <input type="submit" name="goback" value="Voltar"/>
            </form>
        </div>
    </body>
</html>
<?php
    } else {
        header("Location: ../");
    }
 ?>
