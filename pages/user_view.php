<?php
    session_start();
    include_once("system/functions.php");
    $pdo = new_db_connection();
    $query = "SELECT id, firstname, lastname, pic_url, email, current_status, gender FROM users WHERE id = :id";

    $sql = $pdo->prepare($query);
    $sql->bindParam(':id', $_GET['id']);

    try {
        $sql->execute();
        $userinfo = $sql->fetch();

        // Se o utilizador não existir
        if($userinfo == NULL){
            header("Location: index.php");
            die();
        }

    } catch(Exception $e){
        die($e);
    }
    $relationStatus = relationStatus($_SESSION['userid'], $_GET['id']);
 ?>
<html>
    <head>
        <?php getHtmlHead();?>
        <?php echo "<title>".$userinfo['firstname']." ".$userinfo['lastname']."</title>";?>
    </head>
    <body>
        <div id="container">
            <?php
            session_handler();
            if(!isset($_SESSION['userid'])){
                echo "Faça login para ver mais informações.";
            }
            ?>
            <table class='userinfo'>
            <?php
                echo "<tr><td>Imagem</td><td><img height='100px' width='100px' src='".$userinfo['pic_url']."'/>";
                echo "<tr>";
                echo "<td>Nome</td>";
                echo "<td>".$userinfo['firstname']." ".$userinfo['lastname']."</td>";
                echo "</tr>";

                // Se for amigo, for o próprio ou se for ADMIN
                if($relationStatus == 1 || $_SESSION['userid'] == $_GET['id'] || $_SESSION['admin'] == 1){
                    echo "<tr><td>Email</td>";
                    echo "<td>".$userinfo['email']."</td><tr/>";
                    echo "<tr><td>Sexo</td>";
                    echo "<td>".$userinfo['gender']."</td></tr>";
                    echo "<tr><td>O que estou a fazer</td>";
                    if($userinfo['current_status'] == NULL){
                        echo "<td><i>Sem estado</i></td></tr>";
                    }else{
                        echo "<td>".$userinfo['current_status']."</td></tr>";
                    }

                }

                if(isset($_SESSION['userid'])){
                    if ($userinfo['id'] != $_SESSION['userid']){
                        if($relationStatus == 0){        // Não é amigo
                            echo "<a href='system/friendship_ask.php?id=".$userinfo['id']."'>Pedir em amizade</a> para visualizar perfil completo";
                        }elseif ($relationStatus == 1){   // É amigo
                            echo "<a href='system/friendship_delete.php?id=".$userinfo['id']."'>Remover amizade</a>";
                        } else if ($relationStatus == 2){   // Pedido enviado
                            echo "Pedido de amizade enviado";
                        }
                    }
                }
             ?>
            </table>
        </div>
    </body>
</html>
