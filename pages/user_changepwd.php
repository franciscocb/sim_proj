<?php
    session_start();
    if(isset($_SESSION['userid'])){
?>
<html>
    <head>
        <title>Change  password</title>
        <link rel="stylesheet" href="style/layout.css" type="text/css"/>
    </head>
    <body>
        <div id="container">
            <form method="POST" action="system/user_changepwd.php">
                <?php session_handler(); ?>
                <table class="form">
                    <tr>
                        <td>Password actual:</td>
                        <td><input type="password" name="old" required /></td>
                        <td><?php if($_GET['error'] == 1) echo "<p class='error'>Password actual errada</p>"?></td>
                    </tr>
                    <tr>
                        <td>Nova password:</td>
                        <td><input type="password" name="new1" required /></td>
                        <td><?php if($_GET['error'] == 2) echo "<p class='error'>As passwords não coincidem</p>"?></td>
                    </tr>
                    <tr>
                        <td>Confirmar nova password:</td>
                        <td><input type="password" name="new2" required /></td>
                    </tr>
                </table>
                <input type="submit" name="submit" value="Alterar password"/>
                <input type="submit" name="goback" value="Voltar"/>
            </form>
        </div>
    </body>
</html>
<?php
    } else {
        header("Location: index.php");
    }
 ?>
