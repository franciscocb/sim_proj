<?php
    session_start();
    include_once("system/functions.php");

    if(isset($_SESSION['userid'])){
        $pdo = new_db_connection();
        $query = "SELECT * FROM users WHERE id = :id";
        $sql = $pdo->prepare($query);
        $sql->bindParam(':id', $_SESSION['userid']);

        try{
            $sql->execute();
            $user = $sql->fetch();

            $gender = $row['gender'];

        } catch(Exception $e){
            header("Location: index.php");
            die();
        }
 ?>
 <html>
    <head>
        <?php getHtmlHead();?>
    </head>
    <body>
        <div id="container">
            <?php session_handler(); ?>
            <form method="POST" action="system/user_changeinfo.php">
                <table>
                    <tr>
                        <td>Nome:</td>
                        <td><input type="text" name="firstname" value="<?php echo $user['firstname'];?>" required /></td>
                    </tr>
                    <tr>
                        <td>Apelido:</td>
                        <td><input type="text" name="lastname" value="<?php echo $user['lastname'];?>"required /></td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td><input type="text" name="email" value="<?php echo $user['email'];?>" required /></td>
                    </tr>
                    <tr>
                        <td>Sexo:</td>
                        <td>
                         <select  id="gender" name="gender">
                            <option value="Masculino" <?php if($gender==1) { ?> selected="selected" <?php } ?>> Masculino</option>
                            <option value="Feminino" <?php if($gender==2) { ?> selected="selected" <?php } ?> >Feminino</option>
                            <option value="Outro" <?php if($gender==3) { ?> selected="selected" <?php } ?> >Outro</option>
                        </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Password actual:</td>
                        <td><input type="password" name="password" required /></td>
                    </tr>
                </table>
                <input type="submit" name="chinfo" value="Alterar definições"/>
                <input type="submit" name="goback" value="Voltar" formnovalidate />
            </form>
        </div>
    </body>
 </html>
<?php
    } else {
        header("Location: index.php");
    }
 ?>
